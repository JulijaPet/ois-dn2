var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var geslaGledeNaKanal = {};
var uporabljeniKanali = [];

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
//<<<<<<< HEAD
    obdejalZasebnoSporocilo(socket,uporabljeniVzdevki);
//=======
//    obdelajKanal(socket,'Skedenj');
//>>>>>>> Naloga_2_7
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
 //   obdelajKanalZGeslom(socket,'Skedenj','geslo');
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    kdoKlepeta(socket, 'Skedenj');
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {
    kanal: vzdevkiGledeNaSocket[socket.id] + ' @ ' + kanal
  }); 
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var j = 0;
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
  kdoKlepeta(socket,kanal);
}

function obdelajKanal(socket, kanal) {
    var stevec = [];
    var kanalNov = '';
    var geslo = '';
    var j = 0;
    for(var i = 0; i < kanal.length; i++) {
        if (kanal.indexOf('"') >= 0) {
            stevec[j] = kanal.indexOf('"');
            j++;
            kanal = kanal.replace('"', '');
        }    
    }
    if(stevec.length > 0) {
      if (stevec.length == 4) {// kanal z geslom
        kanalNov = kanal.substring(stevec[0],stevec[1]);
        geslo = kanal.substring(stevec[2], stevec[4]);
        var napaka = 0;
        for(var i in trenutniKanal) {
            if(trenutniKanal[i] == kanalNov) {
                napaka = 1;
                var obvestilo = '"Izbrani kanal ' + kanalNov + ' je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom.';
                socket.emit('sporocilo', {besedilo: obvestilo});
                break;
            }
        }
        if (napaka === 0) {
            obdelajKanalZGeslom(socket,kanalNov,geslo);
        }
      } else if (stevec.length == 2) { // kanal, ki ni zasciten z geslom!!
        kanalNov = kanal.substring(stevec[0],stevec[1]);
        var obstaja = 0;
        for(var i in uporabljeniKanali) {
            if(uporabljeniKanali[i] == kanalNov) obstaja = 1;
        }
        if(obstaja == 1) {
            var napacnoGeslo = '"Pridruzitev v kanal ' + kanal + ' ni bilo uspesno, ker je geslo napacno!"';
            socket.emit('sporocilo', {besedilo: napacnoGeslo});
        } else {
          socket.leave(trenutniKanal[socket.id]);
          socket.leave(uporabljeniKanali[socket.id]);
          pridruzitevKanalu(socket, kanal);
          kdoKlepeta(socket,kanal);
        }
      } else socket.emit('sporocilo', {besedilo: 'Napacen vnos kanala ali gesla'});
    } else {
      socket.leave(trenutniKanal[socket.id]);
      socket.leave(uporabljeniKanali[socket.id]);
      pridruzitevKanalu(socket, kanal);
      kdoKlepeta(socket,kanal);
    }  
}

function obdelajKanalZGeslom(socket,kanal,geslo) {
    var kanalZeObstaja = 0;
    for(var i in uporabljeniKanali) {
        if(uporabljeniKanali[i] == kanal) kanalZeObstaja = 1;
    }
    if(kanalZeObstaja == 0) {
        socket.leave(uporabljeniKanali[socket.id]);
        kdoKlepeta(socket,kanal);
        socket.leave(trenutniKanal[socket.id]);
        socket.join(kanal);
        geslaGledeNaKanal[kanal] = geslo;
        uporabljeniKanali[socket.id] = kanal;
        socket.emit('pridruzitevOdgovor', {kanal: vzdevkiGledeNaSocket[socket.id] + ' @ ' + kanal});
        socket.broadcast.to(kanal).emit('sporocilo', {
            besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
        });
    } else {
        if (geslaGledeNaKanal[kanal] == geslo) {
            socket.leave(uporabljeniKanali[socket.id]);
            kdoKlepeta(socket,kanal);
            socket.leave(trenutniKanal[socket.id]);
            socket.join(kanal);
            socket.emit('pridruzitevOdgovor', {kanal: vzdevkiGledeNaSocket[socket.id] + ' @ ' + kanal});
            socket.broadcast.to(kanal).emit('sporocilo', {
                besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
            });
        } else {
            var obvestilo = '"Pridruzitev v kanal ' + kanal + ' ni bilo uspesno, ker je geslo napacno!"';
            socket.emit('sporocilo', {besedilo: obvestilo});
        }
    }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  var kanal = trenutniKanal[socket.id]; // tuki
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
          kanal: kanal // tukiii
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
        besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    obdelajKanal(socket, kanal.novKanal);
  });
}

function obdejalZasebnoSporocilo(socket,uporabljeniVzdevki) {
  socket.on('posljiZasebno', function(zasebno){
    for(var i = 0; i < zasebno.length; i++) {
        zasebno = zasebno.replace('"','');
    }
    var sporocilo = zasebno;
    var vzdevek = zasebno;
    zasebno = zasebno.split(' ');
    var uporanikObstaja = 0;
    var uporabnik = io.sockets.clients();
    for(var i in uporabljeniVzdevki) {
      var indeks = vzdevek.indexOf(uporabljeniVzdevki[i]);
        if(indeks >= 0){
            var ker = uporabljeniVzdevki.indexOf(uporabljeniVzdevki[i]);
            vzdevek = uporabljeniVzdevki[ker];
            if(vzdevkiGledeNaSocket[socket.id] == vzdevek || ker < 0) {
                uporanikObstaja = 1;
                sporocilo = sporocilo.substring(vzdevek.length, sporocilo.length);
                sporocilo = '"Sporocila ' + sporocilo + ' uporabniku z vzdevkom ' + vzdevek + ' ni bilo mogoce poslati."';
                socket.emit('sporocilo', {besedilo: sporocilo});
                break;
            }
            for(var j in uporabnik) {
                if(vzdevkiGledeNaSocket[uporabnik[j].id] == vzdevek) {
                    uporanikObstaja = 1;
                    sporocilo = sporocilo.substring(vzdevek.length, sporocilo.length);
                    var vzdevekPosiljatelj = vzdevkiGledeNaSocket[socket.id];
                    var sporociloPrejemniku = '"' + vzdevekPosiljatelj + ' (zasebno): '+ sporocilo + '"';
                    sporocilo = '"(zasebno za ' + vzdevek + '): ' + sporocilo + '"';
                    uporabnik[j].emit('sporocilo', {besedilo: sporociloPrejemniku});
                    socket.emit('sporocilo', {besedilo: sporocilo});
                    break;
                }
            }
        }
    }
    if (uporanikObstaja === 0) {
        vzdevek = zasebno[0];
        sporocilo = sporocilo.substring(vzdevek.length, sporocilo.length);
        sporocilo = '"Sporocila ' + sporocilo + ' uporabniku z vzdevkom ' + vzdevek + ' ni bilo mogoce poslati."';
        socket.emit('sporocilo', {besedilo: sporocilo});
    }
  });
}
function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}
function kdoKlepeta(socket, kanal) {
  socket.on('posodobitevUporabnikov', function() {
    var array = [];
    var uporabnikiNaKanalu = io.sockets.clients(kanal);
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      array[i] = vzdevkiGledeNaSocket[uporabnikSocketId];
    }
    socket.emit('seznamUporabnikov', {uporabniki: array});
  });
}