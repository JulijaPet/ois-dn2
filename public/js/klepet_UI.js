$('#besede').load('swearWords.txt');
 
function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var array = $('#besede').text();
  sporocilo = klepetApp.swearW(sporocilo, array);
  sporocilo = klepetApp.smiley(sporocilo);
  var sistemskoSporocilo;
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    klepetApp.posljiSporocilo($('#kanal').html(), sporocilo);
    var bool = 0;
    for (var j = 0; j < sporocilo.length; j++) {
      if(sporocilo.charAt(j) == '&' && sporocilo.charAt(j+1) == 'l' && sporocilo.charAt(j+2) == 't') {
       for(var i = 0; i < sporocilo.length; i++) {
          sporocilo = sporocilo.replace('&lt','<');
          sporocilo = sporocilo.replace('&gt','>');
        }
        bool = 1;
        j+=2;
      }
    }  
    if(bool == 1) $('#sporocila').append($('<div style="font-weight: bold"></div>').html(sporocilo));
    else $('#sporocila').append(divElementEnostavniTekst(sporocilo));

   // var array = $('#besede').text();
//    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      $('#kanal').text(rezultat.vzdevek + ' @ ' + rezultat.kanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
  var novElement;
  var boolean = 0;
  for(var j = 0; j < sporocilo.besedilo.length; j++) {
    if(sporocilo.besedilo.charAt(j) == '&' && sporocilo.besedilo.charAt(j+1) == 'l' && sporocilo.besedilo.charAt(j+2) == 't') {
       for(var i = 0; i < sporocilo.besedilo.length; i++) {
          sporocilo.besedilo = sporocilo.besedilo.replace('&lt','<');
          sporocilo.besedilo = sporocilo.besedilo.replace('&gt','>');
       }
       j+=2;
       boolean = 1;
    } 
  }
  if (boolean == 0) novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
  else novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
  $('#sporocila').append(novElement);
  });
  
  socket.on('seznamUporabnikov', function (sporocilo) {
    $('#trenutniUporabniki').empty();
    for (var uporabnik in sporocilo.uporabniki) {
//      sporocilo.uporabniki[uporabnik]
      $('#trenutniUporabniki').append(divElementEnostavniTekst(sporocilo.uporabniki[uporabnik]));
    }
 //   $('#trenutniUporabniki').append(divElementEnostavniTekst(sporocilo.uporabniki));
  });
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);
  
  setInterval(function () {socket.emit('posodobitevUporabnikov')}, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
