var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var index = kanal.indexOf('@');
  if(index >= 0) kanal = kanal.substring(index+2, kanal.length);
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var zasebno = besede.join(' ');
      this.socket.emit('posljiZasebno', zasebno);
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};
Klepet.prototype.swearW = function(besedilo, tabela) {
    var arraySP = besedilo.split(' ');
    var array = tabela.split('\n');
    var zlaBeseda = 0;
    
    for(var i = 0; i < arraySP.length; i++) {
      //dobiš besedo[i] iz sporočila..pol pogedaš če je beseda swearWords...če je potem namesto te besede napišeš toliko * kot je doulga beseda .length
      //arraytSP[i] najprej splitaš da visš če je sličajno pika na konci in pol nakoncu spet nazaj združiš
      var znaki = arraySP[i].split('');
      var znak = '';
      var aliVsebujeZnak = 0;
      for (var j = 0; j < znaki.length; j++) {
        if(znaki[j] == '.') {
          znak = '.';
          znaki[j] = '';
          aliVsebujeZnak = 1;
        } else if(znaki[j] == ',') {
          znak = '.';
          znaki[j] = '';
          aliVsebujeZnak = 1;
        } else if(znaki[j] == '!') {
          znak = '!';
          znaki[j] = '';
          aliVsebujeZnak = 1;
        } else if(znaki[j] == '?') {
          znak = '?';
          znaki[j] = '';
          aliVsebujeZnak = 1;
        } else if(znaki[j] == '-') {
          znak = '-';
          znaki[j] = '';
          aliVsebujeZnak = 1;
        }
      }
      arraySP[i] = znaki.toString();
      for(var j = 0; j < arraySP[i].length;j++) {
        arraySP[i] = arraySP[i].replace(',','');
      }
      for(var j = 0; j < array.length; j++) {
        if(arraySP[i] == array[j]) {
          zlaBeseda = 1;
          break;
        }
      }
      if(zlaBeseda == 1) {
        var zvezdice = new Array(arraySP[i].length);
        for(var j = 0; j < arraySP[i].length; j++) {
          zvezdice[j] = '*'; 
        }
        arraySP[i] = arraySP[i].replace(arraySP[i], zvezdice.toString());
        for(var j = 0; j < arraySP[i].length; j++) {
          arraySP[i] = arraySP[i].replace(',','');
        }
        zlaBeseda = 0;
      }
      arraySP[i] = arraySP[i]+znak;
    }
    //taleto arraySP je treba še kupaj zdruzit in jo shranit v novElement
    besedilo = arraySP.toString();
    for(var i = 0; i < besedilo.length; i++) {
      besedilo = besedilo.replace(',', ' ');
    }
    return besedilo;
}

Klepet.prototype.smiley = function(smajli) {
  var sporocilo = smajli;
  for (var i = 0; i < smajli.length; i++) {
    sporocilo = sporocilo.replace(';)','&ltimg src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"/&gt');
    sporocilo = sporocilo.replace(':)','&ltimg src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"/&gt');
    sporocilo = sporocilo.replace('(y)','&ltimg src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"/&gt');
    sporocilo = sporocilo.replace(':*','&ltimg src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"/&gt');
    sporocilo = sporocilo.replace(':(','&ltimg src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"/&gt');
  }
  return sporocilo;
}
